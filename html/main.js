/*
    Copyright 2019 MeridianGroup (https://meridiangrp.co.uk)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

function sendData(name, data) {
    $.post("http://openidentity/" + name, JSON.stringify(data), function(datab) {
        if (datab != "ok") {
            console.log(datab);
        }
    });
}

var vrId = -1;

window.addEventListener('message', function(event) {
    var data = event.data;

    if (data.request) {
        vrId = data.request;
        $('.player-name').text(data.name);
        $('.view-request').show();
    } else {
        switch (data.view) {
            case 'myId':
                $('.my-id .name').attr('value', data.name);
                $('.my-id .dob').attr('value', data.dob);
                $('.my-id .ref').text(data.ref);
                $('.my-id .issued').attr('value', data.issued);
                $('.my-id .expiry').attr('value', data.expiry);

                $('.my-id').show();
                break;
            case 'otherId':
		console.log(JSON.stringify(data));
                $('.view-id .name').text(data.name);
                $('.view-id .dob').text(data.dob);
                $('.view-id .ref').text(data.ref);
                $('.view-id .issued').text(data.issued);
                $('.view-id .expiry').text(data.expiry);

                $('.view-id').show();
                break;
        }
    }
});

$('.my-id .close').click(function() {
    // Save details as well (just send a message back to client)
    sendData('save-id', {
        name: $('.my-id .name').val(),
        dob: $('.my-id .dob').val(),
        ref: $('.my-id .ref').text(),    
	issued: $('.my-id .issued').val(),
        expiry: $('.my-id .expiry').val()
    });

    $('.my-id').hide();
});

$('.view-id .close').click(function() {
    sendData('close', { });
    $('.view-id').hide();
});

$('.view-request .yes').click(function() {
    // Send data back to client
    sendData('accept-request', {
        id: vrId
    });

    $('.view-request').hide();
});

$('.view-request .no').click(function() {
    $('.view-request').hide();
});

$('.ref-update').click(function() {
    var newRef = '';
    var chars = '0123456789';

    var nameParts = $('.my-id .name').val().split(' ');
    var surname = nameParts[nameParts.length - 1];
    newRef = surname.substr(0, 4).toUpperCase();

    for (var i = 0; i < 12; i++)
	newRef += chars.charAt(Math.floor(Math.random() * chars.length));

    $('.my-id .ref').text(newRef);
});

