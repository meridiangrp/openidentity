--[[
    Copyright 2019 MeridianGroup (https://meridiangrp.co.uk)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
]]

resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

server_script 'sv_openid.lua'
client_script 'cl_openid.lua'

files {
    'html/index.html',
    'html/main.js',
    'html/main.css'
}

server_export 'getIdentities'

ui_page 'html/index.html'
