--[[
    Copyright 2019 MeridianGroup (https://meridiangrp.co.uk)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
]]

-- If any value is itself a table, clone must be updated.
local myId = {
    [ 'name' ] = '',
    [ 'dob' ] = '',
    [ 'ref' ] = '',
    [ 'issued' ] = '',
    [ 'expiry' ] = ''
}

function clone( t )
    local t2 = { }
    for k, v in pairs( t ) do
        t2[ k ] = v
    end
    return t2
end

RegisterCommand( 'myid', function( src, args, rawcmd )
    -- Open UI for editing.
    print( 'Opening my ID' )
    local data = clone( myId )
    data[ 'view' ] = 'myId'
    SetNuiFocus( true, true )
    SendNUIMessage( data )
end, false )

RegisterCommand( 'askid', function( src, args, rawcmd )
    -- Send a request to the server (openid:view_request)
    TriggerServerEvent( 'openid:view_request', args[ 1 ] )
end, false )

RegisterCommand( 'printid', function( src, args, rawcmd )
    print( 'Your identity:' )
    for k, v in pairs( myId ) do
        print( k .. ': ' .. v )
    end
    print( '-----' )
end, false )

RegisterNetEvent( 'openid:view_request' )
AddEventHandler( 'openid:view_request', function( vrId, vrName )
    -- Show UI to confirm/deny request
    SendNUIMessage( {
        [ 'request' ] = vrId,
        [ 'name' ] = vrName
    } )
    SetNuiFocus( true, true )
end )

RegisterNetEvent( 'openid:view_id' )
AddEventHandler( 'openid:view_id', function( viewData )
    -- View the ID specified by viewData
    viewData[ 'view' ] = 'otherId'
    SendNUIMessage( viewData )
    SetNuiFocus( true, true )
end )

RegisterNetEvent( 'openid:request_id' )
AddEventHandler( 'openid:request_id', function( callbackName )
    -- This is the API for getting a client's id
    TriggerServerEvent( callbackName, myId )
end )

RegisterNUICallback( 'accept-request', function( data, cb )
    local id = data.id
    TriggerServerEvent( 'openid:show_id_to', id, myId )
    SetNuiFocus( false, false )
end )

RegisterNUICallback( 'save-id', function( data, cb )
    myId = data
    cb( 'ok' )
    SetNuiFocus( false, false )
    TriggerServerEvent( 'openid:new_id', clone( myId ) )
end )

RegisterNUICallback( 'close', function( data, cb )
    SetNuiFocus( false, false )
end )
