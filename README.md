# OpenIdentity

An identity library for FiveM

## Installation

To install this, either:

- download this repository as an archive and copy the contents to a
  folder named `openidentity` on the FiveM server.
- clone the repository with `git clone https://gitlab.com/meridiangrp/openidentity.git`
  (preferred, as the addon can then be updated with `git pull`)

## Usage (in game)

In game, there are two commands:

- `/myid` - which allows you to edit your own ID
- `/askid <server id>` - requests to view the ID of the specified player, for
  example `/askid 1`

This is the only functionality at this stage. More functionality is intended to be
provided by other addons working on top of this basis.

## Usage (programmatically)

Programatically, OpenIdentity can be called upon to provide a serverside script
with the player's identity as a table. Call the clientside event `openid:request_id`
with the name of a server-side event as an argument. The serverside event will be
pinged with a single argument containing the player's identity as a table.

A simple implementation would be:

```lua
-- On the server
RegisterServerEvent( 'callbackEventName' )
AddEventHandler( 'callbackEventName', function( playerIdentity )
    for k, v in pairs( playerIdentity ) do
        print( '[Player Identity] ' .. k .. ': ' .. v )
    end
end )

-- Triggers for every player!
TriggerClientEvent( 'openid:request_id', -1, 'callbackEventName' )
```

Alternatively, there are a few serverside events for which a handler could be
registered. These are (with arguments shown in parenthases):

- `openid:new_id` (`identity`)
  - Called any time an ID is saved. Note that this ID *could* be a duplicate
    of an ID that already exists. It is recommended to use the ID reference
    as a key to see if the ID actually differs.

## Further Help

For further help, please contact [the author](mailto:lilyh@meridiangrp.co.uk).

## License

Copyright 2019 MeridianGroup (https://meridiangrp.co.uk)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
