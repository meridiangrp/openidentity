--[[
    Copyright 2019 MeridianGroup (https://meridiangrp.co.uk)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
]]

local ids = { }

RegisterServerEvent( 'openid:view_request' )
AddEventHandler( 'openid:view_request', function( plyId )
    -- Someone has requested to view a thing, ping a request to the target
    TriggerClientEvent( 'openid:view_request', plyId, source, GetPlayerName( source ) )
end )

RegisterServerEvent( 'openid:show_id_to' )
AddEventHandler( 'openid:show_id_to', function( target, id )
    for k, v in pairs( id ) do
        print( '[id]: ' .. k .. ': ' .. v )
    end
    TriggerClientEvent( 'openid:view_id', target, id )
end )

RegisterServerEvent( 'openid:new_id' )
AddEventHandler( 'openid:new_id', function( id )
    if id.ref then
        ids[ id.ref ] = id
    end
end )

function getIdentities( )
    return ids
end
